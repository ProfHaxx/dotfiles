import XMonad
import XMonad.Hooks.ManageDocks
import XMonad.Layout.Spacing
import XMonad.Layout.Gaps
import XMonad.Util.CustomKeys

import qualified XMonad.StackSet as W

myLayout = avoidStruts( gaps [(U,10)] $
            spacingRaw False (Border 10 0 10 0) True (Border 0 10 0 10) True $
            layoutHook def)

main = xmonad $ docks def
    { borderWidth = 2
    , terminal = "termite"
    , normalBorderColor = "#80e8c7"
    , focusedBorderColor = "#4ac3ff"
    , modMask = mod4Mask
    , keys = customKeys delkeys inskeys 
    , layoutHook = myLayout }
    where
        delkeys :: XConfig l -> [(KeyMask, KeySym)]
        delkeys XConfig {modMask = modm} =
            [ (modm .|. shiftMask, xK_Return) -- [Old] Terminal
            , (modm              , xK_Return) -- [Old] Swap Focus Master
            ]
            ++
            [(modm .|. m, k) | m <- [0, shiftMask], k <- [xK_w, xK_e, xK_r] ]

        inskeys :: XConfig l -> [((KeyMask, KeySym), X ())]
        inskeys conf@(XConfig {modMask = modm}) = 
            [ ((modm              , xK_Return), spawn $ terminal conf) -- [New] Terminal
            , ((modm .|. shiftMask, xK_Return), windows W.swapMaster)  -- [New] Spawn Focus Master
            , ((modm              , xK_d     ), spawn "exe=`dmenu_path | dmenu` && eval \"exec $exe\"")
            ]
