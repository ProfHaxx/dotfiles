syntax on

" Base Settings
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set nowrap
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set laststatus=2

" Plug
call plug#begin('~/.vim/plugged')

" Color Scheme Plugs
Plug 'ueaner/molokai'
Plug 'cocopon/iceberg.vim'
Plug 'danilo-augusto/vim-afterglow'
Plug 'sainnhe/sonokai'

" Other Plugs
Plug 'itchyny/lightline.vim'
Plug 'vifm/vifm.vim'
Plug 'preservim/nerdtree'
Plug 'ap/vim-css-color'
Plug 'vimwiki/vimwiki'

" Dependencies for Lightline
Plug 'tpope/vim-fugitive'

call plug#end()

colorscheme afterglow

let mapleader = " "

"""""""""""""""""""""""""""""""""""""
" => Splits [Mapping]
"""""""""""""""""""""""""""""""""""""
nnoremap <leader>sh :sp  . <CR>
nnoremap <leader>sv :vsp . <CR>

"""""""""""""""""""""""""""""""""""""
" => Pane Movement [Mapping]
"""""""""""""""""""""""""""""""""""""
nnoremap <leader>j  <C-W><C-J>
nnoremap <leader>h  <C-W><C-H>
nnoremap <leader>k  <C-W><C-K>
nnoremap <leader>l  <C-W><C-L>

"""""""""""""""""""""""""""""""""""""
" => Vifm [Mapping]
"""""""""""""""""""""""""""""""""""""
nnoremap <leader>vf :Vifm       <CR>
nnoremap <leader>vh :SplitVifm  <CR>
nnoremap <leader>vv :VSplitVifm <CR>

"""""""""""""""""""""""""""""""""""""
" => Lightline [Config]
"""""""""""""""""""""""""""""""""""""
let g:lightline = {
\   'colorscheme': 'wombat',
\   'active': {
\       'left': [
\           ['mode', 'paste'],
\           ['gitbranch', 'readonly', 'filename', 'modified']
\       ],
\   },
\   'component_function': {
\       'gitbranch': 'FugitiveHead'
\   },
\ }

"""""""""""""""""""""""""""""""""""""
" => Nerdtree [Mapping]
"""""""""""""""""""""""""""""""""""""
nnoremap <leader>nt :NERDTreeToggle <CR>

"""""""""""""""""""""""""""""""""""""
" => Nerdtree [Config]
"""""""""""""""""""""""""""""""""""""
function! NERDTreeHighlightFile(extension, fg, bg, guifg, guibg)
    exec 'autocmd filetype nerdtree highlight ' . a:extension . ' ctermbg=' . a:bg . ' ctermfg=' . a:fg . ' guibg=' . a:guibg . ' guifg=' . a:guifg
    exec 'autocmd filetype nerdtree syn match ' . a:extension . ' #^\s\+.*' . a:extension . '$#'
endfunction

" Markup Languages
call NERDTreeHighlightFile('md', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('yml', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('html', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('tex', 'blue', 'none', '#3366FF', '#151515')

" Bracket Langs
call NERDTreeHighlightFile('json', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('css', 'blue', 'none', '#3366FF', '#151515')

" Programming Languages
call NERDTreeHighlightFile('php', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('py', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('c', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('ts', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('js', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('jsx', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('hs', 'blue', 'none', '#3366FF', '#151515')

" Out and Non-Edit / Non-Read Files
call NERDTreeHighlightFile('out', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('db', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('aux', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('dvi', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('pdf', 'blue', 'none', '#3366FF', '#151515')

" Configs
call NERDTreeHighlightFile('conf', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('config', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('cfg', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('vim', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('vifm', 'blue', 'none', '#3366FF', '#151515')

" Plain
call NERDTreeHighlightFile('txt', 'blue', 'none', '#3366FF', '#151515')

